API endpoints:

- Get Items: GET http://localhost:5000/api/newsposts

- Get One Item: GET http://localhost:5000/api/newsposts/:id
- Add New Item: POST http://localhost:5000/api/newsposts (body: {})
- Update Item: POST http://localhost:5000/api/newsposts/:id (body: {})
- Delete Item: DELETE http://localhost:5000/api/newsposts/:id



Додати пагінацію до новинного сервісу, та створити DAL навколо fileDB.







Технічні вимоги
Ендпоінт GET /api/newsposts/ має приймати query параметри page, size. Тобто запит може мати вигляд GET /api/newsposts?page=2&size=10. Отримання даних для пагінації має відбуватись на сервері у методі DAL для сутності newsposts.
Створити шар бізнес логіки. Він має викликати методи шару доступу до даних. Наприклад, клас NewspostsService має містити наступні методи:

NewspostsService.getAll(params);
NewspostsService.getById(id);
NewspostsService.create(data);
NewspostsService.update(id, update);
NewspostsService.delete(id)

Створити шар доступу до даних. Він має бути обгорткою навколо методів fileDB. Наприклад, класс NewspostsRepository має містити наступні методи:

NewspostsRepository.getAll(params)
NewspostsRepository.getById(id);
NewspostsRepository.create(data);
NewspostsRepository.update(id, update);
NewspostsRepository.delete(id)

Методи getAll(params) для класів з бізнес логіки та шару доступу до бази даних приймають params. Наразі це може бути об'єкт із наступними полями:

const params = {
  page: 0, // номер сторінки
  size: 10, // кількість елементів на сторінці
}

NewspostsRepository.getAll(params) має повертати пагіновані дані (прочитати усі дані з файлу, пропустити page * size елементів, та повернути наступні size елементів)