import express, { Application } from 'express'
import cors from 'cors';
import Routes from './routes';
import { DatabaseService } from './database.service';
import dotenv from 'dotenv';

const PORT = process.env.PORT || 8000

class App {
  private app: Application

  constructor() {
    this.app = express()
  }

  routing() {
    this.app.get('/', (req, res) => {
      res.sendFile('index.html', {root: './static'})
    })

    Object.keys(Routes).forEach((key) => {
      this.app.use(`/api/${key}`, Routes[key])
    })

  }

  initPlugins() {
    this.app.use(express.json());
    this.app.use(express.static('static'))
    this.app.use(cors())
  }

  async start() {
    if (process.env.NODE_ENV !== 'production') {
        await DatabaseService.createTables()
    }

    this.initPlugins()
    this.routing()

    this.app.listen(PORT, () => {
        console.log(`Server is running on port ${PORT}`)
    })
  }

}

dotenv.config()
const app = new App()
app.start()
