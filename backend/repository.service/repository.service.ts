import { Request, Response } from 'express';
import { DatabaseService } from '../database.service' 

export class BaseRepository {
 
    constructor( ) {
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
    }

    async getItems (table: string, req: Request, res: Response) {

        const page = Number(req.query.page) || 1
        const size = Number(req.query.size) || 4
 
        const data = await DatabaseService.readAll(table, 
            {
                page,
                size,
            });
        return data
    }
    
    async getOneItems (table: string, req: Request, res: Response) {
        const data = await DatabaseService.read(table, Number(req.params.id));
        return data
    }
    
    async addNews (table: string, req: Request, res: Response) {
        const data = await DatabaseService.create(table, req.body);
        return data
    }
    
    async updateNews (table: string, req: Request, res: Response) {
        const data = await DatabaseService.read(table, Number(req.params.id));
        if (!data) {
            return { errorMessage: "News not found" };
        }
        const updated = await DatabaseService.update(table, Number(req.params.id), req.body);
        const updateData = await DatabaseService.read(table, Number(req.params.id));
        return updateData
    }
    
    async deleteNews (table: string, req: Request, res: Response) {
        const data = await DatabaseService.read(table, Number(req.params.id));
        if (!data) {
            return { errorMessage: "News not found" };
        }

        await DatabaseService.delete(table, Number(req.params.id));
        return { message: `${table} was  removed` }
    }
}