import PropTypes from "prop-types";
import styles from './Item.module.scss';
import { Link } from 'react-router-dom';
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import { useDispatch} from "react-redux";
import { fetchItems } from '../../redux/silces/itemsSlice';

const Item = ({ id, name, description }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleDelete = async (e) => {
    e.preventDefault();
    try {
      if (id) {
        const data = await axios.delete(`http://localhost:8000/api/newsposts/${id}`);
        navigate('/');
        console.log(data)
      } 
      await dispatch(fetchItems())

    } catch (error) {
      console.error('Error saving news post:', error);
    } 
  };

  return (
    <Link to={`/${id}`}>
      <div className={styles.root}>
        <div className={styles.contentWrapper}>
          <div className={styles.info}>
            <div className={styles.name}>{name}</div>
            <div className={styles.description}>{description}</div>
          </div>
        </div>
        <div className={styles.btnRoot}>
          <button onClick={handleDelete}>Delete News</button>
        </div>
      </div>
    </Link>
  )
}

Item.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  name: PropTypes.string,
  description: PropTypes.string,
};

Item.defaultProps = {
  id: '',
  name: '',
  description: '',
}

export default Item;