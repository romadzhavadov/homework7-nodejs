import React, { useEffect } from 'react';
import styles from './ItemsContainer.module.scss';
import Item from '../Item/Item';
import {shallowEqual, useSelector, useDispatch } from "react-redux";
import { setPage, fetchItems } from '../../redux/silces/itemsSlice';


const ItemsContainer = () => {

  const items = useSelector(state => state.items.items, shallowEqual);
  const { page, size } = useSelector(state => state.items)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchItems(page, size))
  }, [page]);

  const nextPage = () => {
    if (size > items.length) {
        return;
    }

    dispatch(setPage(page + 1))
  };

  const prevPage = () => {
    if (page === 1) {
        return;
    }
    dispatch(setPage(page - 1))
  };

  return (

    <div className={styles.root}>
      <div className={styles.itemWrap}>
        {items 
          ? 
          items?.map(item => <Item key={item.id} {...item} />) 
          : 
          (<p style={{textAlign: 'center'}}>Завантаження...</p>)
        }
      </div>
      <div className={styles.btnWrap}>
          <button onClick={prevPage}>Prev</button>
          <button onClick={nextPage}>Next</button>
        </div>
    </div>

  )
}

export default ItemsContainer;