import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styles from './AddEditNewsPage.module.scss';
import axios from 'axios';
import { shallowEqual, useSelector, useDispatch} from "react-redux";
import { fetchItems } from '../../redux/silces/itemsSlice';

const AddEditNewsPage = () => {

  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const items = useSelector(state => state.items.items, shallowEqual);
  console.log(items)

  const [title, setTitle] = useState('');
  const [text, setText] = useState('');

useEffect(() => {
  if (id) {
    const data = items.find(item => item.id === parseInt(id));
    if (data) {
      setTitle(data.name);
      setText(data.description);
    }
  }
}, [id, items]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newsPost = {id: id, name: title, description: text };
    try {
      if (id) {
        const newsData = await axios.put(`http://localhost:8000/api/newsposts/${id}`, newsPost);
        console.log(newsData)
      } else {
        await axios.post('http://localhost:8000/api/newsposts', newsPost);
        console.log(newsPost)
      }
      await dispatch(fetchItems())
      navigate('/');
    } catch (error) {
      console.error('Error saving news post:', error);
    } 
  };

  return (
    <div className={styles.container}>
      <h2>{id ? 'Редагування' : 'Створення'} новини</h2>
      <form onSubmit={handleSubmit}>
        <div className={styles.wrap}>
          <label className={styles.item}>Заголовок:</label>
          <input className={styles.item} type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
        </div>
        <div className={styles.wrap}>
          <label className={styles.item}>Текст:</label>
          <textarea className={styles.item} value={text} onChange={(e) => setText(e.target.value)}></textarea>
        </div>
        <div className={styles.btnGroup}>
          <button type="submit">Зберегти</button>
          <button onClick={() => navigate('/')}>Повернутися до списку новин</button>
        </div>
      </form>
    </div>
  );
};

export default AddEditNewsPage;