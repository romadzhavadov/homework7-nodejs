import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [],
  loading: false,
  error: null,
  page: 1,
  size: 4
}

const itemsSlice = createSlice({
  name: "items",
  initialState,

  reducers: {
    setItems: (state, action) => {
      state.items = action.payload;
    },

    setLoading(state, action) {
      state.loading = action.payload;
    },

    setError(state, action) {
      state.error = action.payload;
    },

    setPage: (state, action) => {
      state.page = action.payload;
      state.size = action.payload > 1 ? state.size : 4;
    },

    setSize: (state, action) => {
      state.size = action.payload;
    },
  }
});

export const { setItems, setLoading, setError, setPage, setSize} = itemsSlice.actions;

export const fetchItems = (page, size) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
  
    const res = await fetch(`http://localhost:8000/api/newsposts?page=${page}&size=${size}.`);

    if (!res.ok) {
      throw new Error('Failed to fetch items');
    }
    const { data } = await res.json();

    if (data.items.length === 0) {
      dispatch(setPage(page-1));
    }

    dispatch(setItems(data.items));
  } catch (error) {
    console.error('Error fetching items:', error);
    dispatch(setError(error.message));
  } finally {
    dispatch(setLoading(false));
  }
}

export default itemsSlice.reducer;

