import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: null,
  description: null,
  loading: false,
}

const newSlice = createSlice({
  name: "news",
  initialState,

  reducers: {
    setNews: (state, action) => {
      state.name = action.payload.name;
      state.description = action.payload.description;
    },
    setLoading(state, action) {
      state.loading = action.payload;
    },
  }
});

export const { setNews, setLoading } = newSlice.actions;


export const fetchOneItems = (id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const res = await fetch(`http://localhost:8000/api/newsposts/${id}`);
    console.log(res)
    if (!res.ok) {
      throw new Error('Failed to fetch items');
    }
    const data = await res.json();
    console.log(data)
    dispatch(setNews(data.data));
  } catch (error) {
    console.error('Error fetching items:', error);
  } finally {
    dispatch(setLoading(false));
  }
}
export default newSlice.reducer;

